export async function getUserInfo () {
  const { data: res } = await this.$http.post('/getUserInfo')
  // console.log(res.data)
  this.userInfo = res.data
}
