import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import axios from 'axios'
import { getUserInfo } from './assets/js/common'
import tinymce from 'tinymce'
import VueTinymce from '@packy-tang/vue-tinymce'

// 配置根路径
// axios.defaults.baseURL = 'http://localhost:8088/'
axios.defaults.baseURL = 'http://139.196.238.103:10084/'
// axios.defaults.baseURL = 'http://cn1.utools.club:41547/'
// axios.defaults.baseURL = 'http://192.168.137.1:10084/'
// axios.defaults.baseURL = 'http://220.162.244.13:34494/'
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
Vue.prototype.$http = axios

Vue.prototype.$getUserInfo = getUserInfo

Vue.config.productionTip = false
Vue.prototype.$tinymce = tinymce
Vue.use(VueTinymce)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
