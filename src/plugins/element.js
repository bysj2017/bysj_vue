import Vue from 'vue'
import {
  Aside,
  Button, Col, Container, Form, FormItem, Header,
  Input, Main, Menu, MenuItem, MenuItemGroup, Message, Card,
  Radio, Row, Submenu, Breadcrumb, BreadcrumbItem,
  Table, TableColumn, RadioGroup, Select, Option,
  Upload, MessageBox, Pagination, Tooltip, Dialog, DatePicker,
  Tag, Link, Cascader, Tabs, TabPane, Step, Steps, ButtonGroup, Progress, Rate, Switch
} from 'element-ui'

Vue.use(Button)
Vue.use(ButtonGroup)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)
Vue.use(Submenu)
Vue.use(Col)
Vue.use(Row)
Vue.use(Container)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Header)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Card)
Vue.use(Select)
Vue.use(Option)
Vue.use(Upload)
Vue.use(Pagination)
Vue.use(Tooltip)
Vue.use(Dialog)
Vue.use(DatePicker)
Vue.use(Tag)
Vue.use(Link)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Cascader)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Step)
Vue.use(Steps)
Vue.use(Progress)
Vue.use(Rate)
Vue.use(Switch)
Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
