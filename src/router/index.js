import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import '../assets/css/global.css'
import '../assets/css/darkBlueBackground.css'
import Home from '@/components/Stu'
import MyCourse from '@/views/student/MyCourse'
import Edit from '@/views/student/Edit'
import Inquire from '@/views/student/Inquire'
import Teach from '@/components/Teach'
import CourseManagement from '@/views/teacher/CourseManagement'
import ExpManagement from '@/views/teacher/ExpManagement'
import StudentManagement from '@/views/teacher/StudentManagement'
import TemplatePub from '@/views/teacher/TemplatePub'
import MarkReport from '@/views/teacher/MarkReport'
import Evaluation from '@/views/teacher/Evaluation'
import Admin from '@/components/Admin'
import ExpMng from '@/views/admin/ExpMng'
import PeopleMng from '@/views/admin/PeopleMng'
import CourseMng from '@/views/admin/CourseMng'
import ReportMng from '@/views/admin/ReportMng'
import Simulation from '../views/student/Simulation'
import Guide from '../views/student/Guide'
import Remote from '../views/student/Remote'
import MyExp from '../views/student/MyExp'
import VideoTest from '../views/student/VideoTest'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/stu',
      component: Home,
      redirect: '/guide',
      children: [
        {
          path: '/guide',
          component: Guide
        },
        {
          path: '/myCourse',
          component: MyCourse
        },
        {
          path: '/myExp',
          component: MyExp
        },
        {
          path: '/simulation',
          component: Simulation
        },
        {
          path: '/remote',
          component: Remote
        },
        {
          path: '/videoTest',
          component: VideoTest
        },
        {
          path: '/edit',
          name: 'edit',
          component: Edit
        },
        {
          path: '/inquire',
          component: Inquire
        }
      ]
    },
    {
      path: '/teach',
      component: Teach,
      redirect: '/course_mg',
      children: [
        {
          path: '/course_mg',
          component: CourseManagement
        },
        {
          path: '/exp_mg',
          component: ExpManagement
        },
        {
          path: '/student_mg',
          component: StudentManagement
        },
        {
          path: '/push',
          component: TemplatePub
        },
        {
          path: '/mark',
          component: MarkReport
        },
        {
          path: '/eval',
          component: Evaluation
        }
      ]
    },
    {
      path: '/admin',
      component: Admin,
      redirect: '/people',
      children: [
        {
          path: '/people',
          component: PeopleMng
        },
        {
          path: '/course',
          component: CourseMng
        },
        {
          path: '/expMng',
          component: ExpMng
        },
        {
          path: '/report',
          component: ReportMng
        }
      ]
    }
  ]
})
// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  // next() 放行  next('/login') 强制跳转
  if (to.path === '/login') return next()
  // 获取token
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})
export default router
