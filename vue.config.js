module.exports = {
  chainWebpack: config => {
    config.module
      .rule('md')
      .test(/\.md$/)
      .use('vue-loader')
      .loader('vue-loader')
      .end()
      .use('vue-markdown-loader')
      .loader('vue-markdown-loader/lib/markdown-compiler')
      .options({
        raw: true
      })
  },
  configureWebpack: (config) => {
    config.module.rules.push({
      test: /\.(swf|ttf|eot|svg|woff(2))(\?[a-z0-9]+)?$/,
      loader: 'file-loader'
    })
  }
}

module.exports = {
  // 选项...
  chainWebpack: config => {
    config.module
      .rule('swf')
      .test(/\.swf$/)
      .use('url-loader')
      .loader('url-loader')
      .tap(options => {
        return {
          limit: 10000
        }
      })
  }
}
